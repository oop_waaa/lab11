package com.patcharin.week11;

public class Crocodile extends Animal implements Crawable, Swimable{
    public Crocodile(String name) {
        super(name, 4);
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString()+ " sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this.toString()+ " eat.");
    }

    @Override
    public String toString() {
        return "Crocodile ("+this.getName()+")";
    }

    @Override
    public void craw() {
        System.out.println(this+ " craw.");
        
    }
    @Override
    public void swim() {
        System.out.println(this+ " swim.");
        
    }
}
