package com.patcharin.week11;


public class App 
{
    public static void main( String[] args )
    {
        Bird bird1 = new Bird("Gegee");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        bird1.run();

        Plane boeing1 = new Plane("ฺBoeing" , "Rosaroi");
        boeing1.takeoff();
        boeing1.fly();
        boeing1.landing();

        Superman joe = new Superman("Joe");
        joe.takeoff();
        joe.fly();
        joe.landing();
        joe.walk();
        joe.run();
        joe.eat();
        joe.sleep();
        joe.swim();

        Human rose = new Human("Rose");
        rose.eat();
        rose.sleep();
        rose.walk();
        rose.run();
        rose.swim();

        Bat bruce = new Bat("Bruce");
        bruce.eat();
        bruce.sleep();
        bruce.takeoff();
        bruce.fly();
        bruce.landing();
        
        Snake dang = new Snake("Dang");
        dang.eat();
        dang.sleep();
        dang.craw();

        Crocodile Chalawan = new Crocodile("Chalawan");
        Chalawan.eat();
        Chalawan.sleep();
        Chalawan.craw();
        Chalawan.swim();

        Fish nemo = new Fish("Nemo");
        nemo.eat();
        nemo.sleep();
        nemo.swim();

        Rat remy = new Rat("Remy");
        remy.eat();
        remy.sleep();
        remy.walk();
        remy.run();

        Dog pong = new Dog("Pong");
        pong.eat();
        pong.sleep();
        pong.walk();
        pong.run();

        Cat frame = new Cat("Frame");
        frame.eat();
        frame.sleep();
        frame.walk();
        frame.run();


        Flyable[] flyables = {bird1, boeing1, joe, bruce};
        for(int i = 0; i<flyables.length;i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();

        }
        Walkable[] walkables = {bird1, joe, rose, remy, pong, frame};
        for(int i = 0; i<walkables.length;i++) {
            walkables[i].walk();
            walkables[i].run();
        }

        Crawable[] crawables = {dang, Chalawan};
        for(int i = 0; i<crawables.length;i++) {
            crawables[i].craw();
            
        }

        Swimable[] swimables = { joe, Chalawan, rose, nemo};
        for(int i = 0; i<swimables.length;i++) {
            swimables[i].swim();
            
        }
    }
 }
